//
//  Models.swift
//  Listen
//

import Foundation

struct MusicList:Codable,Equatable {
    var songName : String? = ""
    var singer:String? = ""
    var duration : String? = ""
    var url : String? = ""
    var category:String? = ""
    var pictureLink:String? = ""
}
