//
//  PlayerViewController.swift
//  Listen
//

import Foundation
import UIKit
import AVKit

class PlayerViewController: UIViewController {
    
    //MARK: IBOulets
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var songImage: UIImageView!
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var singerName: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var playPauseBtn: UIButton!
    
    //MARK: Variable Declaration
    var player:AVPlayer?
    var playerLayer:AVPlayerLayer?
    var musicList: [MusicList] = []
    var selectedMusic: MusicList?
    var currentPosition = 0
    var puseTime: CMTime = .zero
    
    override func viewDidLoad() {
        playerSetup(audioUrl: selectedMusic?.url ?? "")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let item = object as? AVPlayerItem, keyPath == #keyPath(AVPlayerItem.status) else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        switch item.status {
        case .failed:
            showAlert(title: "", message: "Something went wrong")
        case .readyToPlay:
            playerReadyToPlay()
        default:
            break
        }
    }

    //MARK: IBActions
    @IBAction func actionOnPlay(_ sender: Any) {
        if player?.isPlaying ?? false {
            pauseAction()
            playPauseBtn.setImage(UIImage(named: "play"), for: .normal)
        } else {
            playAction()
            playPauseBtn.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    @IBAction func actionOnPrevious(_ sender: Any) {
        if currentPosition > 0 && currentPosition < musicList.count - 1 {
            currentPosition -= 1
            changeSong(music: musicList[currentPosition])
        }
    }
    
    @IBAction func actionOnNext(_ sender: Any) {
        if currentPosition < musicList.count - 1 {
            currentPosition += 1
            changeSong(music: musicList[currentPosition])
        }
    }
    
    @IBAction func actionOnSlider(_ sender: UISlider) {
        let seekingCM = CMTimeMake(value: Int64(sender.value * Float(puseTime.timescale)), timescale: puseTime.timescale)
        player?.seek(to: seekingCM)
        startTime.text = seekingCM.durationText
    }
    
    @IBAction func actionOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func playerSetup(audioUrl : String) {
        guard let url = URL(string: audioUrl) else { return }
        let playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        player?.currentItem?.addObserver(self, forKeyPath: "status", options: .new, context: nil)
        playerLayer = AVPlayerLayer(player: player!)
        playAction()
        setUpProgressView()
    }
    
    func setUpProgressView() {
        songName.text = selectedMusic?.songName
        singerName.text = selectedMusic?.singer
        songImage.loadFrom(URLAddress: selectedMusic?.pictureLink ?? "")
        progressSlider.addTarget(self, action: #selector(onSliderAction(slider:event:)), for: .valueChanged)
        player?.addProgressObserver { time  in
            if self.player?.currentItem?.status == .readyToPlay {
                self.progressSlider.minimumValue = 0
                self.progressSlider.maximumValue = Float(self.player?.currentItem?.duration.seconds ?? 0.0)
                self.progressSlider.value = Float(time.seconds)
                self.startTime.text = time.durationText
            }
        }
    }
    
    @objc private func onSliderAction(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                guard let currentTime = player?.currentItem?.currentTime() else {return}
                self.puseTime = currentTime
            case .ended:
                playAction()
            default:
                break
            }
        }
    }
    
    func playerReadyToPlay() {
        self.endTime.text = player?.currentItem?.duration.durationText
    }
    
    func changeSong(music: MusicList) {
        guard let url = URL(string: music.url ?? "") else { return }
        let newPlayerItem = AVPlayerItem(url: url)
        player?.replaceCurrentItem(with: newPlayerItem)
        songName.text = music.songName
        singerName.text = music.singer
        songImage.loadFrom(URLAddress: music.pictureLink ?? "")
        playAction()
    }
    
    func playAction() {
        player?.play()
    }
    
    func pauseAction() {
        player?.pause()
    }
}

