//
//  ViewController.swift
//  Listen
//

import UIKit

class ViewController: UIViewController  {
    
    //MARK: IBOutlets
    @IBOutlet weak var musicTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Variable Declarations
    var musicList: [MusicList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMusicList()
    }
    
    //MARK: API Call
    /// To Get MusicList
    func getMusicList() {
        guard let url = URL(string: "https://c00b-14-97-127-234.in.ngrok.io/song/getallsongs") else { return }
        self.activityIndicator.startAnimating()
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let response = try decoder.decode([MusicList].self, from: data)
                    self.musicList = response
                    print(self.musicList)
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        self.musicTableView.reloadData()
                    }
                } catch {
                    print(error)
                }
            } else if let error = error {
                self.activityIndicator.stopAnimating()
                print("HTTP Request Failed \(error)")
            }
        }
        task.resume()
    }
    
    //MARK: Navigate to playerView
    func navigateToPlayerView(selectedMusic: MusicList) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        vc.musicList = self.musicList
        vc.selectedMusic = selectedMusic
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musicList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicCell", for: indexPath) as! MusicCell
        cell.songName.text = musicList[indexPath.row].songName ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToPlayerView(selectedMusic: musicList[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class MusicCell: UITableViewCell {
    @IBOutlet weak var songName: UILabel!
}

